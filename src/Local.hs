module Local where

import XMonad (X)

extraKeys :: [(String, X())]
extraKeys = []

touchpadNames :: [String]
touchpadNames = ["ELAN0412:00 04F3:316F Touchpad", "DELL0817:00 044E:121F Touchpad"]
